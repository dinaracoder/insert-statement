-- Adding new film to film table
INSERT INTO "film" ("title", "description", "release_year",
				  "language_id", "rental_duration", "rental_rate", 
				  "length", "replacement_cost", "rating")
VALUES ('Harry Potter and the Philosopher''s Stone', 'The first part in the Harry Potter film series', 2001,
		1, 14, 4.99,
		152, 20.99, 'PG');

-- Adding actors to actor table
INSERT INTO "actor" ("first_name", "last_name") 
VALUES ('Daniel', 'Radcliffe'),
       ('Rupert', 'Grint'),
       ('Emma', 'Watson');
       
-- Adding film actors to film_actor table
INSERT INTO "film_actor" ("actor_id", "film_id")
VALUES ((SELECT "actor_id" 
		 FROM "actor" 
		 WHERE "first_name" = 'Daniel'), 
        (SELECT "film_id" 
		 FROM "film" 
		 WHERE "title" = 'Harry Potter and the Philosopher''s Stone')),
		
       ((SELECT "actor_id" 
		 FROM "actor" 
		 WHERE "first_name" = 'Rupert'),
        (SELECT "film_id" 
		 FROM "film" 
		 WHERE "title" = 'Harry Potter and the Philosopher''s Stone')),
		
       ((SELECT "actor_id" 
		 FROM "actor" 
		 WHERE "first_name" = 'Emma'),
        (SELECT "film_id" 
		 FROM "film"
		 WHERE "title" = 'Harry Potter and the Philosopher''s Stone'));
        
-- Adding new film to store by inventory        
INSERT INTO "inventory" ("film_id", "store_id")
VALUES ((
	SELECT "film_id"
	FROM "film"
	WHERE "title" = 'Harry Potter and the Philosopher''s Stone'), 1);
	
	
	
	
	
    SELECT film.title, inventory.store_id, actor.first_name ||' '|| actor.last_name as fullname
	FROM film
	JOIN inventory USING(film_id)
	JOIN film_actor USING(film_id)
	JOIN actor USING(actor_id)
	WHERE film.title LIKE '%Harry%';
	
